const webpack = require('webpack')
const {merge} = require('webpack-merge')
const base = require('./webpack.base')
const paths = require('./paths')

module.exports = merge(base, {
  mode: 'development',
  devtool: 'eval-cheap-source-map',
  devServer: {
    historyApiFallback: true,
    contentBase: paths.build,
    open: true,
    compress: true,
    hot: true,
    port: 4040
  },
  module: {
    rules: [
      {
        test: /\.(s[ac]ss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
})
